Rails.application.routes.draw do
  resources :line_items
  resources :carts, only: [:show, :destroy]
  ActiveAdmin.routes(self)
  devise_for :users
  root to: 'restaurants#index'
  resources :restaurants do
  	resources :dishes
  end
  post 'line_items' => "line_items#create"
  delete 'carts/:id' => "carts#destroy"
end






