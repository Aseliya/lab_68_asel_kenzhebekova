class Dish < ApplicationRecord
	belongs_to :restaurant
	has_one_attached :image
	validate :file_type

	def file_type
		if image.attached? && !image.content_type.in?(%w('image/jpeg' 'image/jpg'))
			errors.add(:image, 'Is not an image')
		end
	end
end
