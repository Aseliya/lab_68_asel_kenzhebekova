class Cart < ApplicationRecord
	has_many :line_items, dependent: :destroy
	has_many :dishes, through: :line_items	

	def total_cart
		line_items.to_a.sum { |dish| dish.total_sum }
	end
end


