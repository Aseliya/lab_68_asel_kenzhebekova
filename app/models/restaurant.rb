class Restaurant < ApplicationRecord
	has_many :dishes, dependent: :destroy
	has_one_attached :picture
	validate :file_type

	def file_type
		if picture.attached? && !picture.content_type.in?(%w('image/jpeg' 'image/jpg'))
			errors.add(:picture, 'Is not an image')
		end
	end

end
