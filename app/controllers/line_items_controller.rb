class LineItemsController < ApplicationController
	def create
  dish = Dish.find(params[:dish_id])
  cart_of_dishes = @cart_of_dishes

  if cart_of_dishes.dishes.include?(dish)
    @line_item = cart_of_dishes.line_items.find_by(:dish_id => dish)
    @line_item.quantity += 1
  else
    @line_item = LineItem.new
    @line_item.cart = cart_of_dishes
    @line_item.dish = dish
  end

  @line_item.save
  redirect_back(fallback_location: root_path)
end

  private

    def line_item_params
      params.require(:line_item).permit(:dish_id, :cart_id, :quantity)
    end
end

