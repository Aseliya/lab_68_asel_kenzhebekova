class RestaurantsController < ApplicationController
	def index
		@restaurants = Restaurant.all
	end

	def show
		@restaurant = Restaurant.find(params[:id])
		@cart = @cart_of_dishes
		@dish = Dish.find(params[:id])

		
	end
	private

	def restaurant_params
		params.require(:restaurant).permit(:name, :picture, :description)
	end
end