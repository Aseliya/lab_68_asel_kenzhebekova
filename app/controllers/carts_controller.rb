class CartsController < ApplicationController
	def show
		@cart = @cart_of_dishes
	end

	def destroy
		@cart = @cart_of_dishes
		@cart.destroy
		session[:cart_id] = nil
		redirect_back(fallback_location: root_path)
	end
end

