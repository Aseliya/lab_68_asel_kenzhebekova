class ApplicationController < ActionController::Base
	before_action :cart_of_dishes

	def cart_of_dishes
		if session[:cart_id]
			cart = Cart.find_by(:id => session[:cart_id])
			if cart.present?
				@cart_of_dishes = cart
			else
				session[:cart_id] = nil
			end
		end

		if session[:cart_id] == nil
			@cart_of_dishes = Cart.create
			session[:cart_id] = @cart_of_dishes.id
		end
	end
end
