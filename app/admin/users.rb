ActiveAdmin.register User do
	form do |f|
		f.inputs do
			f.input :name
			f.input :email
			f.input :address
			f.input :phone
			f.input :password


		end
		f.actions
	end

	index do

		selectable_column
		id_column
		column :name
		column :email
		column :address
		column :phone
		actions
	end

	show do
		attributes_table do
		row :name
		row :email
		row :address
		row :phone
		end
		active_admin_comments
	end

	permit_params :name, :email, :address, :phone, :password, :list, :of, :attributes, :on, :model 

end
