ActiveAdmin.register Restaurant do
form do |f|
		f.inputs do
			f.input :name
			f.input :description
			if restaurant.picture.attached?

			f.input :picture,
			:as => :file,
			:hint => image_tag(
				url_for(
					f.object.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
					)
				)
		else f.input :picture, :as => :file
		end
		f.actions
	end
end

	index do

		selectable_column
		id_column
		column :picture do |restaurant|
			image_tag restaurant.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
		end
		column :name do |restaurant|
			link_to restaurant.name, admin_restaurant_path(restaurant)
		end
		column :description
		actions
	end

	show do
		attributes_table do
			row :picture do |restaurant|
				image_tag restaurant.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0' })
			end
			row :name
			row :description
		end
		active_admin_comments
	end

	permit_params :dish_id, :name, :description, :picture, :list, :of, :attributes, :on, :model 

end
