ActiveAdmin.register Dish do

form do |f|
		f.inputs do
			f.input :title
			f.input :price
			f.input :description
			f.select :restaurant_id, Restaurant.all.map{|r| [r.name, r.id]}
			if dish.image.attached?

			f.input :image, as: :file,
			:hint => image_tag(
				url_for(
					f.object.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
					)
				)
		else f.input :image, as: :file
		end
		f.actions
	end
end
index do

		selectable_column
		id_column
		column :image do |dish|
			image_tag dish.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
		end
		column :title do |dish|
			link_to dish.title, admin_dish_path(dish)
		end
		column :description
		column :price

		actions
	end

	show do
		attributes_table do
			row :image do |dish|
				image_tag dish.image.variant(combine_options: { gravity: 'Center', crop: '50x50+0' })
			end
			row :title
			row :price
			row :description
		end
		active_admin_comments
	end
	permit_params :restaurant_id, :title, :description, :price, :image, :list, :of, :attributes, :on, :model 

end
