# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
user = User.create(
	name:'Vasiliy', 
	address: 'Abay street', 
	phone: '87771234567', 
	password: '123456',
	email: 'vasiliy@mail.ru')

admin = User.create(
	name: 'admin',
	address: 'Tole bi street',
	phone: '87779886532', 
	email: 'admin@mail.ru', 
	password: '123456', 
	admin: true)

def copy_image_fixture(dish, file)
  fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures', "#{file}.jpg")
  dish.image.attach(io: File.open(fixtures_path), filename: "#{file}.jpg")
end

def copy_picture_fixture(restaurant, file)
	fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures', "#{file}.jpg")
	restaurant.picture.attach(io: File.open(fixtures_path), filename: "#{file}.jpg")
end


uzbechka = Restaurant.create(name: 'Узбечка', description: Faker::Lorem.sentence)

plov = Dish.create(title: 'Плов', price: '100', description: Faker::Lorem.sentence, restaurant_id: uzbechka.id)
manty = Dish.create(title: 'Манты', price: '200', description: Faker::Lorem.sentence, restaurant_id: uzbechka.id)
lagman = Dish.create(title: 'Лагман', price: '300', description: Faker::Lorem.sentence, restaurant_id: uzbechka.id)
copy_image_fixture(plov, 'plov')
copy_image_fixture(manty, 'manty')
copy_image_fixture(lagman, 'lagman')
copy_picture_fixture(uzbechka, '1')

europa = Restaurant.create(name: 'Европа', description: Faker::Lorem.sentence)

stake = Dish.create(title: 'Стейк', price: '400', description: Faker::Lorem.sentence, restaurant_id: europa.id)
losos = Dish.create(title: 'Лосось с овощами', price: '500', description: Faker::Lorem.sentence, restaurant_id: europa.id)
jarovnya = Dish.create(title: 'Мясо на жаровне', price: '600', description: Faker::Lorem.sentence, restaurant_id: europa.id)
copy_image_fixture(stake, 'stake')
copy_image_fixture(losos, 'losos')
copy_image_fixture(jarovnya, 'jarovnya')
copy_picture_fixture(europa, '2')

fast_food = Restaurant.create(name: 'Fast Food', description: Faker::Lorem.sentence)

pizza = Dish.create(title: 'Пицца "Маргарита"', price: '100', description: Faker::Lorem.sentence, restaurant_id: fast_food.id)
sushi = Dish.create(title: 'Суши "Филадельфия"', price: '200', description: Faker::Lorem.sentence, restaurant_id: fast_food.id)
hotdog = Dish.create(title: 'Хот-дог', price: '50', description: Faker::Lorem.sentence, restaurant_id: fast_food.id)
copy_image_fixture(pizza, 'pizza')
copy_image_fixture(sushi, 'sushi')
copy_image_fixture(hotdog, 'hot-dog')
copy_picture_fixture(fast_food, '3')